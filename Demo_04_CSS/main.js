//Ajouter/Retirer/Verifier/Toggle Class
// $("#change-color").on('click', () => {
//     if($("#color").hasClass("tomato")) {
//         $("#color").removeClass("tomato");
//         $("#color").addClass("chartreuse");
//     }
//     else {
//         $("#color").removeClass("chartreuse");
//         $("#color").addClass("tomato");
//     }
// })

$("#change-color").on('click', () => {
    $("#color").toggleClass("chartreuse");
    $("#color").toggleClass("tomato");    
})


$("#hide").on('click', () => {
    // $("#color").hide(2000);
    $('#color').fadeOut(1000)
})


$("#show").on('click', () => {
    // $("#color").show(2000);
    $('#color').fadeIn(1000);

})