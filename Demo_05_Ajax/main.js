const ZELDA_URL = "https://botw-compendium.herokuapp.com/api/v3/compendium/category/monsters";

$.ajax({
    //Url sur laquelle on fait la requête
    url : ZELDA_URL,

    //Méthode de la requête (Post, get, put, patch, delete)
    method : "GET",

    //Type des données récupérées (html, json, xml, etc...)
    dataType : "json",

    //3 callbacks :
        //Si la requête fonctionne
    success : (rep, status) => {

        console.log("[SUCCESS] rep : ", rep);

        //On parcourt les 10 premiers monstres
        for(let i = 0; i < 10; i++) {

            const MONSTER_CARD = $(`<div>
                <img src="${rep.data[i].image}" alt="${rep.data[i].name}" />
                <h2>${rep.data[i].name}</h2>
                <p>${rep.data[i].description}</p>
            </div>`);
            $('#monsters').append(MONSTER_CARD);

        }
        console.log("[SUCCESS] status : ", status);
    },
        //Si erreur pendant la requête
    error : (res, status, error) => {
        console.log("[ERROR] res : ", res);
        console.log("[ERROR] status : ", status);
        console.log("[ERROR] error : ", error);

    },
        //Quand la requête se termine
    complete : (res, status) => {
        console.log("[COMPLETE] res : ", res);
        console.log("[COMPLETE] status : ", status);

    }
})

// Equivalent avec la méthode moderne fetch :
// fetch(ZELDA_URL)
//     .then((res) => {
//         res.json().then((rep) => {
//             console.log(rep.data);
//         })
//     })
//     .catch((err) => {})
//     .finally(() => {})