// jQuery()
// Pour récupérer un élément :
console.log( $("h1") );
console.log( $("#intro") );
console.log( $(".green") )
// L'équivalent en JS avant l'apparition de querySelector, étaient getByTagName, getById, getByClassName
// L'équivalent en JS après l'apparation de querySelector (qui a contribué à la mise à mort de JQuery) -> document.querySelectorAll & document.querySelector

// Ce qu'on obtient avec notre selecteur $(), c'est un objet jquery qui réprésente une collection : On peut parcourir les éléments avec [] commme un tableau
// ! Attention, ce n'est pas un tableau (pas de foreach)
// ! Attention, même si vous ne récupérez qu'un élément, vous aurez quand même une collection, juste qu'il y a un seul élément dedans
console.log($(".green")[1]);
console.log( $("#intro")[0]); //Même si un seul élément, on récup le premier élément

// ---------------------- //
// Récupérer plusieurs éléments avec une query
const GREEN_ELEM = $(".green");
// Dans l'objet jquery qu'on a récupéré, on peut faire une selection plus affinée
console.log(GREEN_ELEM.find(".a-trouver"));

// Pour parcourir chacun des élément, comme ce n'est pas un tableau et qu'on ne peut pas utiliser le forEach et le for of, qu'on connait, il existe une fonction each, qu'on peut appliquer
console.log("EACH()");
GREEN_ELEM.each(function() {
    console.log($(this)); 
    //$(this) contient chacun des éléments à chaque tour de boucle
    //! Attention, c'est aussi un objet jQuery, si vous voulez le manipuler, il faudra rajouter [0] pour récup le premier
})
