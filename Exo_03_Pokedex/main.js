let nextRequest = null;
let previousRequest = null;

loadPokemons("https://pokeapi.co/api/v2/pokemon");

$("#prev").on("click", () => {
    loadPokemons(previousRequest);
})
$("#next").on("click", () => {
    loadPokemons(nextRequest);
})


function loadPokemons(requestUrl) {
    
    $.ajax({
        url : requestUrl,
        method : "GET",
        dataType : "json",
        success : (res) => {

            console.log(res);
            // Récupérer le next et le prévious
            nextRequest = res.next;
            previousRequest = res.previous;
            // Si next est null, on désactive le button
            // Si previous est null, on désactive le button
            $("#prev").attr("disabled", previousRequest == null);
            $("#next").attr("disabled", nextRequest == null);

            // Mettre les 20pkm dans la liste
            $("#pkm-list").html("");
            res.results.forEach(pkm => {
                const li = $("<li> </li");
                li.text(pkm.name);
                li.on("click", () => {
                    detailPokemon(pkm.url);
                })

                $("#pkm-list").append(li);
            })
            
        },
    })
}


function detailPokemon(detailUrl) {
    
    $.ajax({
        url : detailUrl,
        method : "GET",
        dataType : "json",
        success : (res) => {
            console.log(res);
            $("#pkm-name").text(res.name);
            $("#pkm-name").css("color", `var(--${res.types[0].type.name})`);

            $("#pkm-image").attr("src", res.sprites.other.dream_world.front_default);
            $("#pkm-image").attr("alt", res.name);
            $("#pkm-num").text(res.id);

            //infos
            $("#pkm-height").text(res.height * 10);
            $("#pkm-weight").text(res.weight / 10);

            //types
            $("#pkm-types").empty();
            res.types.forEach(type => {
                const p = $("<p> </p>");
                p.text(type.type.name)
                $("#pkm-types").append(p);
            })
            
        }
    })
}   