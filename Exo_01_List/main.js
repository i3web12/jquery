// ? Au clic du bouton :
    // ■ vérifier que le champ n'est pas vide et pas composé d'espaces
    // ■ SI ok, ajouter le champ dans la liste
    // ■ Si pas ok, afficher message d'erreur
    // ! ■ Vider le champ une fois ajouté et retirer le message d'erreur si plus besoin

$("#add-product").on('click', () => {

    if($("#product").val().trim() !== '' ){

        $("#liste").append(`<li> ${$("#product").val().trim()} </li>`);
        $("#product").val('');
        $("#error-product").text(''); //hide

    }
    else {

        $("#error-product").text("Ce champ est requis"); //show

    }

})
