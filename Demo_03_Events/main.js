//! Evènements
$("#write-name").on('click', () => {
    $("#name").val("Aude");
})

// Equivalent en js
// document.getElementById("write-name").addEventListener('click', () => {
//     document.getElementById("name").value = "Aude";
// })

//! Créer/ajouter du contenu
// En Js
// const LISTE = document.getElementById("liste");

// const LI = document.createElement("li");
// LI.innerText = "Pouet";

// LISTE.appendChild(LI);

// En jQuery
$("#liste").append("<li> Pouet </pi>");

//! WRAPPINGS
$("nav a").wrap("<li> </li>"); //Pour envelopper tous les liens d'un li
$("nav li").wrapAll("<ul> </ul>") //Pour envelopper tous les li d'un ul

$("nav a").unwrap().unwrap() // Pour enlever les balises autour de chaque lien


// ! Les attributs
// En js
// const ATTRIBUTE_INPUT = document.getElementById("att");
// ATTRIBUTE_INPUT.setAttribute("placeholder", "Mon attribut")
// ATTRIBUTE_INPUT.setAttribute("readonly", true)

// en jquery
// ! On peut en ajouter plusieurs d'un coup
$("#att").attr({ placeholder : "Mon attribut", readonly : true })


// ! Supprimer/Vider/Cloner 
//Supprime l'élément
// $("nav").remove(); 

//Vide l'élément de tout son contenu html mais le garde lui
// $("nav").empty(); 

// Clone l'élément
// ! Attention il est créé mais pas ajouté dans le html, il faudra donc le faire
// $("body").append($("nav").clone()); 