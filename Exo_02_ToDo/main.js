// Ajout de l'évènement Click sur le button du formulaire :
$("#task-create").on("click", () => {
    //On récupère chacune des informations de notre formulaire :
    const taskName = $("#task-name").val();
    const taskUrg = $("#task-urgent").val();
    const taskDesc = $("#task-desc").val();

    //Si le formulaire est rempli :
    if(taskName.trim() != "" && taskUrg.trim() != "" && taskDesc.trim() != "") {
        
        // Ajout dans la liste des tâches
        const taskTitle = $("<h3></h3>");
        taskTitle.text(taskName);

        const taskDescription = $("<p></p>");
        taskDescription.text(taskDesc);
        taskDescription.addClass("hidden");
        taskDescription.addClass("card-desc");
        // taskDescription.hide(); //Attention style inline

        const taskDevelop = $("<span></span>");
        taskDevelop.text("V");
        taskDevelop.addClass("develop");
        taskDevelop.on("click", () => {
            // taskDescription.show(); //Attention style inline
            taskDescription.toggleClass("hidden");
        })

        const taskTitleFull = $("<div></div>");
        taskTitleFull.append(taskTitle);
        taskTitleFull.append(taskDevelop);
        taskTitleFull.addClass("card-title");


        const taskCard = $("<div></div>");
        taskCard.append(taskTitleFull);
        taskCard.append(taskDescription);
        taskCard.addClass("card");
        //taskUrg contient "urgent" ou "not-urgent", c'est pile les noms de nos classes
        taskCard.addClass(taskUrg);

        if(taskUrg === "urgent") {
            //Ajoute en premier dans l'élément
            $("#task-list").prepend(taskCard);
        }
        else {
            //Ajoute en dernier dans l'élément
            $("#task-list").append(taskCard);
        }
        
    }
    else {
        console.log("PAS OK");
    }
})

