//! Modifier/Récupérer le HTML des éléments
// ! $().html()

//Récupération (attention, n'affche que le contenu du premier élément)
//console.log($('.pink').html());

//Remplace le innerHTML de tous les éléments par celui passé en paramètre
//$('.pink').html('<p> Pink </p>'); 

//Pour récupérer le contenu déjà existant et rajouter le nouveau
//$('.pink').html($(".pink").html() + '<p> Rose </p>' ); 

//! Modifier/Récupérer le TextContent des éléments
// ! $().text()

//Récupérer le textContent : Attention ! Récupère le texte de tous les éléments et les concatène pour n'en faire qu'une seule chaine à la fin
console.log($('.pink').text()); 

//Remplacer le text par un autre pour chacun des élements
$('.pink').text("Pink");

//! Modifier/Récupérer les valeurs des inputs/textarea etc
// ! $().val()

// Récupérer les valeurs du prémier élément
console.log($(".my-input").val());

// Remplacer toutes les valeurs de chacun des éléments par une nouvelle valeur
$(".my-input").val("Pouet")


// Afficher nombre d'éléments dans l'objet jQuery :
console.log($(".my-input").length);